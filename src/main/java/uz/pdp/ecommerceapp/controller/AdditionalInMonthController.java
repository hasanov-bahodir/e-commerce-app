package uz.pdp.ecommerceapp.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import uz.pdp.ecommerceapp.model.AdditionalInMonth;
import uz.pdp.ecommerceapp.service.AdditionalInMonthService;

import java.util.UUID;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/additionalInMonth")
public class AdditionalInMonthController {

    private final AdditionalInMonthService additionalInMonthService;

    @GetMapping
    public ResponseEntity<?> getAllAdditionalInMonths() {
        return additionalInMonthService.getAllAdditionalInMonths();
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> getAdditionalInMonthId(@PathVariable UUID id) {
        return additionalInMonthService.getAdditionalInMonthId(id);
    }

    @PostMapping
    public ResponseEntity<?> addBrand(@RequestBody AdditionalInMonth additionalInMonth) {
        return additionalInMonthService.addAdditionalInMonth(additionalInMonth);
    }

    @PutMapping("/edit/{id}")
    public ResponseEntity<?> addBrand(@PathVariable UUID id, @RequestBody AdditionalInMonth additionalInMonth) {
        return additionalInMonthService.editAdditionalInMonth(id, additionalInMonth);
    }

    @DeleteMapping("delete/{id}")
    public ResponseEntity<?> deleteBrand(@PathVariable UUID id) {
        return additionalInMonthService.deleteAdditionalInMonth(id);
    }


}
