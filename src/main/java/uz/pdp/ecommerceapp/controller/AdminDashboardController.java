package uz.pdp.ecommerceapp.controller;
//Sevinch Abdisattorova 04/14/2022 9:47 PM

import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import uz.pdp.ecommerceapp.service.AdminDashboardService;

import java.util.UUID;

import static uz.pdp.ecommerceapp.utils.Constants.DEFAULT_PAGE_SIZE;

@RestController
@RequestMapping("/api/dashboard")
@RequiredArgsConstructor
public class AdminDashboardController {

    private final AdminDashboardService adminDashboardService;


    @PreAuthorize(value = "hasAuthority('PRODUCTS_COUNT_BY_CATEGORY')")
    @GetMapping("/products-count-by-category")
    public ResponseEntity<?> showProductsOfCategory(
            @RequestParam(name = "size", defaultValue = DEFAULT_PAGE_SIZE) int size,
            @RequestParam(name = "page", defaultValue = "1") int page,
            @RequestParam(name = "search", defaultValue = "") String search
    ) {
        return adminDashboardService.countProductsByCategory(search, size, page);
    }


    @PreAuthorize(value = "hasAuthority('PRODUCTS_OF_CATEGORY')")
    @GetMapping("/products-of-category")
    public ResponseEntity<?> getAllProductsOfCategory(
            @RequestParam UUID categoryId,
            @RequestParam(name = "size", defaultValue = DEFAULT_PAGE_SIZE) int size,
            @RequestParam(name = "page", defaultValue = "1") int page,
            @RequestParam(name = "search", defaultValue = "") String search) {
        return adminDashboardService.getAllProductOfCategory(categoryId, page, size, search);
    }


    @PreAuthorize(value = "hasAuthority('GET_MOST_FAVOURITE_PRODUCTS')")
    @GetMapping("/most-favourite-product")
    public ResponseEntity<?> getTheMostFavouriteProducts(
            @RequestParam(name = "size", defaultValue = DEFAULT_PAGE_SIZE) int size,
            @RequestParam(name = "page", defaultValue = "1") int page,
            @RequestParam(name = "search", defaultValue = "") String search
    ) {
        return adminDashboardService.getTheMostFavouriteProducts(size, search, page);
    }


    @PreAuthorize(value = "hasAuthority('GET_USERS_LIKED_PRODUCT')")
    @GetMapping("/users-liked-product")
    public ResponseEntity<?> getUsersLikedProducts(
            @RequestParam UUID productId,
            @RequestParam(name = "size", defaultValue = DEFAULT_PAGE_SIZE) int size,
            @RequestParam(name = "page", defaultValue = "1") int page,
            @RequestParam(name = "search", defaultValue = "") String search) {
        return adminDashboardService.getUsersLikedThisProduct(productId, size, search, page);
    }

    @PreAuthorize(value = "hasAuthority('COUNT_USERS_BY_ROLE')")
    @GetMapping("/role-users")
    public ResponseEntity<?> countUsersByRole(
    ) {
        return adminDashboardService.countUsersByRole();
    }


    @PreAuthorize(value = "hasAuthority('GET_USERS_BY_ROLE')")
    @GetMapping("users-by-role")
    public ResponseEntity<?> getUsersByRole(
            UUID roleId,
            @RequestParam(name = "size", defaultValue = DEFAULT_PAGE_SIZE) int size,
            @RequestParam(name = "page", defaultValue = "1") int page,
            @RequestParam(name = "search", defaultValue = "") String search
    ) {
        return adminDashboardService.getUsersByRole(roleId, size, page, search);
    }
}
