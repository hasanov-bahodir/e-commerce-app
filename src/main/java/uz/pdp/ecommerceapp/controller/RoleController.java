package uz.pdp.ecommerceapp.controller;
// Bahodir Hasanov 4/14/2022 11:35 PM

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import uz.pdp.ecommerceapp.dto.RoleDto;
import uz.pdp.ecommerceapp.payload.ApiResponse;
import uz.pdp.ecommerceapp.repository.RoleRepository;
import uz.pdp.ecommerceapp.service.RoleService;

@RestController
@RequestMapping("api/role")
public class RoleController {
    @Autowired
    RoleService roleService;

    @PostMapping
    public ResponseEntity<?> saveNewRole(@RequestBody RoleDto roleDto) {
        ApiResponse apiResponse = roleService.saveRole(roleDto);
        return ResponseEntity.status(apiResponse.isStatus()?200:409).body(apiResponse);
    }
}
