package uz.pdp.ecommerceapp.controller;


import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import uz.pdp.ecommerceapp.service.FavoriteProductService;

import java.util.UUID;


@RestController
@RequiredArgsConstructor
@RequestMapping(value = "/api/favorite-product")
public class FavoriteController {
    private final FavoriteProductService favoriteProductService;

    @PostMapping
    public ResponseEntity<?> addFavoriteProduct(
            @RequestParam(value = "userId") UUID userId,
            @RequestParam(value = "productId") UUID productId) {
        return favoriteProductService.addFavoriteProduct(userId, productId);
    }


    @DeleteMapping
    public ResponseEntity<?> deleteFavoriteProduct(
            @RequestParam(value = "productId") UUID productId,
            @RequestParam(value = "userId") UUID userId) {
        return favoriteProductService.deleteFavoriteProductByProductId(productId, userId);
    }

    @GetMapping("/{userId}")
    public ResponseEntity<?> showAllFavoriteProductByUserId(@PathVariable UUID userId) {
        return favoriteProductService.getAllFavoriteProductByUserId(userId);
    }
}
