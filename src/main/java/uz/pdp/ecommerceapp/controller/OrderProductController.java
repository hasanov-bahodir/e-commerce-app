package uz.pdp.ecommerceapp.controller;


//Sevinch Abdisattorova 04/13/2022 06:34 AM

import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import uz.pdp.ecommerceapp.service.OrderProductService;
import uz.pdp.ecommerceapp.service.OrderService;

import javax.servlet.http.HttpServletRequest;
import java.util.UUID;


@RestController
@RequestMapping("/api/order-product")
@RequiredArgsConstructor
public class OrderProductController {

    private final OrderProductService orderProductService;


    @PreAuthorize(value = "hasAuthority('GET_ALL_ORDER_PRODUCTS_OF_ORDER')")
    @GetMapping("/{orderId}")
    public ResponseEntity<?> getAllOrderProducts(
            @PathVariable UUID orderId) {
        return orderProductService.getAllOrderProducts(orderId);
    }

}
