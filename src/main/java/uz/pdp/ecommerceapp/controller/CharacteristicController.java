package uz.pdp.ecommerceapp.controller;
//Sevinch Abdisattorova 04/11/2022 9:46 PM

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import uz.pdp.ecommerceapp.dto.CharacteristicDto;
import uz.pdp.ecommerceapp.service.CharacteristicService;

import java.util.UUID;

@RestController
@RequiredArgsConstructor
@RequestMapping(value = "/api/characteristics")
public class CharacteristicController {


    private final CharacteristicService characteristicService;


    @PreAuthorize(value = "hasAuthority('GET_CHARACTERISTICS')")
    @GetMapping
    public ResponseEntity<?> showAllCharacteristics() {
        return characteristicService.getAllCharacteristics();
    }


    @PreAuthorize(value = "hasAuthority('ADD_CHARACTERISTIC')")
    @PostMapping
    public ResponseEntity<?> addNewCharacteristicOrValuesToExistingCharacteristic(
            @RequestBody CharacteristicDto characteristicDto) {
        return characteristicService.addNewCharacteristicOrValuesToExistingCharacteristic(characteristicDto);
    }

    @PreAuthorize(value = "hasAuthority('EDIT_CHARACTERISTIC')")
    @PutMapping("/{id}")
    public ResponseEntity<?> editCharacteristic(
            @PathVariable UUID id,
            CharacteristicDto characteristicDto) {
        return characteristicService.editCharacteristicById(id, characteristicDto);

    }

    @PreAuthorize(value = "hasAuthority('DELETE_CHARACTERISTIC')")
    @DeleteMapping("/{id}")
    public HttpEntity<?> deleteCharacteristicById(@PathVariable UUID id) {
        return characteristicService.deleteCharacteristicById(id);
    }
}
