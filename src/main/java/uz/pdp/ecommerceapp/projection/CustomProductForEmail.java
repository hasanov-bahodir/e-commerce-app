package uz.pdp.ecommerceapp.projection;
//Sevinch Abdisattorova 04/14/2022 1:14 AM


import org.springframework.beans.factory.annotation.Value;
import uz.pdp.ecommerceapp.model.AttachmentContent;

import java.util.UUID;

public interface CustomProductForEmail {

    UUID getId();

    UUID getPhotoId();

    String getName();

    Double getPrice();


    @Value("#{@attachmentContentRepository.findByAttachmentId(target.photoId)}")
    AttachmentContent getPhoto();


}
