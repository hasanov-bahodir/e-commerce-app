package uz.pdp.ecommerceapp.projection;

import java.util.UUID;

public interface CustomTransHistoryForUser {


    UUID getOrderId();

    Integer getOrderSerialNumber();

    UUID getPayTypeId();

    String getPayTypeName();

}
