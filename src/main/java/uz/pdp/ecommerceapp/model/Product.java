package uz.pdp.ecommerceapp.model;
//Sevinch Abdisattorova 04/11/2022 2:06 PM


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.PackagePrivate;
import uz.pdp.ecommerceapp.model.template.AbsEntity;

import javax.persistence.*;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Data
@PackagePrivate
@Entity(name = "products")
public class Product extends AbsEntity {

    String name;

    @ManyToOne
    @JoinColumn(name = "brand_id")
    Brand brand;
    @ManyToOne
    @JoinColumn(name = "category_id")
    Category category;

    String description;

    Double price;

    @ManyToMany(cascade = CascadeType.MERGE)
    @JoinTable(
            name = "products_characteristics",
            joinColumns = {@JoinColumn(name = "product_id")},
            inverseJoinColumns = {@JoinColumn(name = "characteristics_value_id")})
    List<CharacteristicValue> characteristicsValues;

    @OneToMany(cascade = CascadeType.MERGE)
    @JoinTable(
            name = "products_attachments",
            joinColumns = {@JoinColumn(name = "product_id")},
            inverseJoinColumns = {@JoinColumn(name = "attachment_id")})
    List<Attachment> attachments;

}
