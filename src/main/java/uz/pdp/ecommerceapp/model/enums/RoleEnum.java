package uz.pdp.ecommerceapp.model.enums;

public enum RoleEnum {
    ROLE_USER,
    ROLE_ADMIN,
    ROLE_DIRECTOR
}
