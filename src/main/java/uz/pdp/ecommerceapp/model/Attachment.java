package uz.pdp.ecommerceapp.model;
//Sevinch Abdisattorova 04/11/2022 1:56 PM

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.PackagePrivate;
import uz.pdp.ecommerceapp.model.template.AbsEntity;

import javax.persistence.Entity;

@AllArgsConstructor
@NoArgsConstructor
@Data
@PackagePrivate
@Entity(name = "attachments")
public class Attachment extends AbsEntity {
    String name;
    String contentType;
    Long size;
}
