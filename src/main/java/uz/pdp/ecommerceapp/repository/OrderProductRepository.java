package uz.pdp.ecommerceapp.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import uz.pdp.ecommerceapp.model.Order;
import uz.pdp.ecommerceapp.model.OrderProduct;
import uz.pdp.ecommerceapp.projection.CustomOrderProduct;

import java.util.List;
import java.util.UUID;

@Repository
public interface OrderProductRepository extends JpaRepository<OrderProduct, UUID> {


    @Query(nativeQuery = true, value = "select cast(oi.id as varchar) as id,\n" +
            "       oi.quantity            as quantity,\n" +
            "       oi.total_price         as totalPrice,\n" +
            "       cast(p.id as varchar)  as productId,\n" +
            "       p.name                 as productName\n" +
            "from orders o\n" +
            "         join order_items oi on o.id = oi.order_id\n" +
            "         join products p on p.id = oi.product_id\n" +
            "where o.id = :orderId")
    List<CustomOrderProduct> getAllOrderProductsOfOrderId(UUID orderId);
}
