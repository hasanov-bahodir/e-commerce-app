package uz.pdp.ecommerceapp.repository;
//Sardor {4/12/2022}{ 4:02 AM}

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import uz.pdp.ecommerceapp.model.Category;
import uz.pdp.ecommerceapp.model.ProductRates;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Repository
public interface ProductRateRepository extends JpaRepository<ProductRates, UUID> {

    List<ProductRates> getAllByProductId(UUID productId);

    boolean existsByUserIdAndProductId(UUID user_id, UUID product_id);

    @Query(nativeQuery = true,value = "select orders.user_id as user_id,oi.product_id as product_id\n" +
            "from orders\n" +
            "         join order_items oi on orders.id = oi.order_id\n" +
            "         join transaction_histories th on orders.id = th.order_id\n" +
            "where orders.user_id = :userId\n" +
            "  and oi.product_id = :productId")
    boolean isPurchasedProduct(UUID userId, UUID productId);
    // TODO: 4/14/2022  





}
