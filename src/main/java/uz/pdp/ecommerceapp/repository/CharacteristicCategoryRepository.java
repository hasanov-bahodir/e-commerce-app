package uz.pdp.ecommerceapp.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import uz.pdp.ecommerceapp.model.CharacteristicCategory;
import uz.pdp.ecommerceapp.projection.CustomCharacteristicCategory;

import java.util.List;
import java.util.UUID;

@Repository
public interface CharacteristicCategoryRepository extends JpaRepository<CharacteristicCategory, UUID> {

    CharacteristicCategory findByName(String name);

    @Query(nativeQuery = true, value = "select cast(id as varchar), name\n" +
            "from characteristics_categories")
    List<CustomCharacteristicCategory> getAllCategories();
}
