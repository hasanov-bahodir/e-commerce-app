package uz.pdp.ecommerceapp.payload;

import lombok.RequiredArgsConstructor;
import org.springframework.boot.CommandLineRunner;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import uz.pdp.ecommerceapp.model.*;
import uz.pdp.ecommerceapp.model.enums.PermissionName;
import uz.pdp.ecommerceapp.model.enums.RoleEnum;
import uz.pdp.ecommerceapp.repository.*;

import java.util.*;

@Component
@RequiredArgsConstructor
public class DataLoader implements CommandLineRunner {

    private final AdditionalInMonthRepository additionalInMonthRepository;
    private final BrandRepository brandRepository;
    private final FirstPaymentPercentageRepository firstPaymentPercentageRepository;
    private final ValueRepository valueRepository;
    private final CharacteristicValueRepository characteristicValueRepository;
    private final CharacteristicRepository characteristicRepository;
    private final CharacteristicCategoryRepository characteristicCategoryRepository;
    private final UserRepository userRepository;
    private final CategoryRepository categoryRepository;
    private final ProductRepository productRepository;
    private final PayTypeRepository payTypeRepository;
    private final RoleRepository roleRepository;
    private final PermissionRepository permissionRepository;
    private final PasswordEncoder passwordEncoder;

    @org.springframework.beans.factory.annotation.Value("${spring.sql.init.mode}")
    String initMode;

    @Override
    public void run(String... args) throws Exception {
        if (initMode.equals("always")) {

            /**
             * CREATE ROLE ADMIN 
             */
            PermissionName[] permissionsName = PermissionName.values();
            Set<Permission> permissions = new HashSet<>();

            for (PermissionName permission : permissionsName) {
                permissions.add(new Permission(permission.name()));
            }
            permissionRepository.saveAll(permissions);
            Set<Role> roles = new HashSet<>();
            Role role = new Role(
                    RoleEnum.ROLE_ADMIN,
                    permissions
            );
            roles.add(role);
            roleRepository.saveAll(roles);
            userRepository.save(new User("Admin", "+998901234567", "admin@gamil.com", passwordEncoder.encode("1"), true, roles));


            /**
             * CREATE ROLE USER 
             */
            Set<Permission> permissionUserList = new HashSet<>();
            permissionUserList.add(new Permission(PermissionName.GET_PRODUCT.name()));
            permissionUserList.add(new Permission(PermissionName.GET_PRODUCT_BY_ID.name()));
            permissionUserList.add(new Permission(PermissionName.GET_PRODUCT_CHARACTERISTIC_BY_ID.name()));
            permissionRepository.saveAll(permissionUserList);
            Set<Role> userRoles = new HashSet<>();
            Role roleUser = new Role(
                    RoleEnum.ROLE_USER,
                    permissionUserList
            );
            userRoles.add(roleUser);
            roleRepository.saveAll(userRoles);
            userRepository.save(new User("User", "+998997777777", "user@gamil.com", passwordEncoder.encode("1"), true, userRoles));

            /**
             * SAVE BRANDS
             */
            Brand hp = brandRepository.save(new Brand("HP"));
            Brand samsung = brandRepository.save(new Brand("Samsung"));
            Brand apple = brandRepository.save(new Brand("Apple"));
            Brand acer = brandRepository.save(new Brand("Acer"));
            Brand asus = brandRepository.save(new Brand("Asus"));
            Brand dell = brandRepository.save(new Brand("Dell"));
            Brand microsoft = brandRepository.save(new Brand("Microsoft"));

            /**
             * PRODUCT CATEGORY
             */
            Category electronics = categoryRepository.save(new Category("Electronics"));
            Category laptops = categoryRepository.save(new Category("Laptops", electronics));

            /**
             * SAVE ADDITIONAL IN MONTH
             */
            double percent = 5.0;
            for (int i = 3; i <= 12; i++) {
                additionalInMonthRepository.save(new AdditionalInMonth(i, percent));
                percent += 5.0;
            }

            /**
             * SAVE FIRST PAYMENT PERCENTAGE
             */
            firstPaymentPercentageRepository.save(new FirstPaymentPercentage(25.0));

            /**
             * SAVE VALUE
             * processor
             */
            List<Value> processorList = new ArrayList<>();

            Value value1 = valueRepository.save(new Value("Intel Core i7-1195G7"));
            Value value2 = valueRepository.save(new Value("Intel Core i5-1135G7"));
            Value value3 = valueRepository.save(new Value("Intel Core i5-10310U"));
            Value value4 = valueRepository.save(new Value("Intel® Core™ i7-10510U 1.8 GHz"));
            Value value5 = valueRepository.save(new Value("Intel® Core™ i5-10210U 1600 MGts"));

            processorList.add(value1);
            processorList.add(value2);
            processorList.add(value3);
            processorList.add(value4);
            processorList.add(value5);

            /**
             * screen type
             */
            List<Value> screenList = new ArrayList<>();

            Value hd = valueRepository.save(new Value("HD"));
            Value fullHd = valueRepository.save(new Value("Full HD"));
            Value ipc = valueRepository.save(new Value("Full HD, IPC"));

            screenList.add(hd);
            screenList.add(fullHd);
            screenList.add(ipc);

            /**
             * screen size
             */
            List<Value> screenSizeList = new ArrayList<>();

            Value value6 = valueRepository.save(new Value("13"));
            Value value7 = valueRepository.save(new Value("14"));
            Value value8 = valueRepository.save(new Value("15.6"));
            Value value9 = valueRepository.save(new Value("16"));
            Value value10 = valueRepository.save(new Value("17"));

            screenSizeList.add(value6);
            screenSizeList.add(value7);
            screenSizeList.add(value8);
            screenSizeList.add(value9);
            screenSizeList.add(value10);
            /**
             * characterCategory
             */
            CharacteristicCategory main_characteristics = characteristicCategoryRepository.save(new CharacteristicCategory("Main characteristics"));

            /**
             * save characteristics
             */
            Characteristic processor = characteristicRepository.save(new Characteristic("Processor", true, main_characteristics));
            Characteristic screen = characteristicRepository.save(new Characteristic("Screen", true, main_characteristics));
            Characteristic screenSize = characteristicRepository.save(new Characteristic("Screen size", true, main_characteristics));
            List<CharacteristicValue> characteristicValueList = new ArrayList<>();

            for (Value processorValue : processorList) {
                CharacteristicValue save = characteristicValueRepository.save(new CharacteristicValue(processor, processorValue));
                characteristicValueList.add(save);
            }

            for (Value screenValue : screenList) {
                CharacteristicValue save = characteristicValueRepository.save(new CharacteristicValue(screen, screenValue));
                characteristicValueList.add(save);
            }

            for (Value screenSizeValue : screenSizeList) {
                CharacteristicValue save = characteristicValueRepository.save(new CharacteristicValue(screenSize, screenSizeValue));
                characteristicValueList.add(save);
            }
            productRepository.save(new Product("MacBook Air", apple, laptops, "Apple MacBook Air 2020 is a macOS laptop with a 13.30-inch display that has a resolution of 2560x1600 px", 1500.0, characteristicValueList, new ArrayList<>()));
            payTypeRepository.save(new PayType("stripe", 3D));
        }
    }
}
