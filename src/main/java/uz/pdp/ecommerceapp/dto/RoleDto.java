package uz.pdp.ecommerceapp.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.Set;
import java.util.UUID;

// Bahodir Hasanov 4/14/2022 11:36 PM
@AllArgsConstructor
@NoArgsConstructor
@Data
public class RoleDto {
    @NotBlank
    private String name;
    private Set<UUID> permissions;
}
