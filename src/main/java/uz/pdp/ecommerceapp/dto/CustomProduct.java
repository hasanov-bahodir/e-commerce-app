package uz.pdp.ecommerceapp.dto;
//Sevinch Abdisattorova 04/14/2022 2:50 AM


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.PackagePrivate;

import java.util.UUID;

@AllArgsConstructor
@NoArgsConstructor
@Data
@PackagePrivate
public class CustomProduct {

    UUID id;

    String photo;

    String name;

    Double price;
}
