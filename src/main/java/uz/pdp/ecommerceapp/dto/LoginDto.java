package uz.pdp.ecommerceapp.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;

// Bahodir Hasanov 4/14/2022 3:00 PM
@Data
@AllArgsConstructor
@NoArgsConstructor
public class LoginDto {

    @NotNull(message = "please enter your phone number with code")
    private String phoneNumber;

    @NotNull(message = "please enter password")
    private String password;


}
