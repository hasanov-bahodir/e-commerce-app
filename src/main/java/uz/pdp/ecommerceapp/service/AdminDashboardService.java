package uz.pdp.ecommerceapp.service;
//Sevinch Abdisattorova 04/14/2022 9:48 PM


import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import uz.pdp.ecommerceapp.projection.*;
import uz.pdp.ecommerceapp.repository.ProductRepository;
import uz.pdp.ecommerceapp.repository.UserRepository;

import java.time.LocalDate;
import java.time.Month;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Objects;
import java.util.UUID;

@Service
@RequiredArgsConstructor
public class AdminDashboardService {

    private final UserRepository userRepository;
    private final ProductRepository productRepository;



    public ResponseEntity<?> countProductsByCategory(String search,
                                                     int size,
                                                     int page) {
        Pageable pageable = PageRequest.of(
                page - 1,
                size
        );

        Page<CustomProductsCountByCategory> productsCountByCategory = productRepository
                .getProductsCountByCategory(pageable, search);

        return ResponseEntity.ok(productsCountByCategory);
    }


    public ResponseEntity<?> getAllProductOfCategory(UUID categoryId,
                                                     int page,
                                                     int size,
                                                     String search) {

        Pageable pageable = PageRequest.of(
                page - 1,
                size
        );


        Page<ProductProjection> allProductOfCategory = productRepository
                .getAllProductOfCategory(pageable, categoryId, search);

        return ResponseEntity.ok(allProductOfCategory);
    }


    public ResponseEntity<?> getTheMostFavouriteProducts(int size,
                                                         String search,
                                                         Integer page) {
        Pageable pageable = PageRequest.of(
                page - 1,
                size
        );

        Page<ProductProjectionForCountOfFavouriteProducts> theMostFavouriteProducts = productRepository
                .getTheMostFavouriteProducts(pageable, search);

        return ResponseEntity.ok(theMostFavouriteProducts);
    }


    public ResponseEntity<?> getUsersLikedThisProduct(UUID productId,
                                                      int size,
                                                      String search,
                                                      int page) {

        Pageable pageable = PageRequest.of(
                page - 1,
                size
        );

        Page<CustomUser> usersLikedThisProduct = userRepository.getUsersLikedThisProduct(pageable, productId, search);
        return ResponseEntity.ok(usersLikedThisProduct);

    }


    public ResponseEntity<?> countUsersByRole() {


        List<CustomUserStatsByRole> userStatsByRoles = userRepository
                .countUsersByRole();

        return ResponseEntity.ok(userStatsByRoles);

    }


    public ResponseEntity<?> getUsersByRole(UUID roleId, int size, int page, String search) {

        Pageable pageable = PageRequest.of(
                page - 1,
                size
        );

        Page<CustomUser> userStatsByRoles = userRepository
                .getUsersByRole(pageable, roleId, search);

        return ResponseEntity.ok(userStatsByRoles);

    }
}
