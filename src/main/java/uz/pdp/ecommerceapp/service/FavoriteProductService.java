package uz.pdp.ecommerceapp.service;//@AllArgsConstructor

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import uz.pdp.ecommerceapp.model.FavoriteProduct;
import uz.pdp.ecommerceapp.model.Product;
import uz.pdp.ecommerceapp.model.User;
import uz.pdp.ecommerceapp.repository.FavoriteProductRepository;
import uz.pdp.ecommerceapp.repository.ProductRepository;
import uz.pdp.ecommerceapp.repository.UserRepository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

//@NoArgsConstructor
//@Data
//@Entity
//import static uz.sardor.Main.*;
//Sardor {4/12/2022}{ 4:02 AM}
@RequiredArgsConstructor
@Service
public class FavoriteProductService {

    private final FavoriteProductRepository favoriteProductRepository;
    private final UserRepository userRepository;
    private final ProductRepository productRepository;

    public ResponseEntity<?> addFavoriteProduct(UUID userId, UUID productId) {
        Optional<User> optionalUser = userRepository.findById(userId);
        Optional<Product> optionalProduct = productRepository.findById(productId);
        if (!optionalProduct.isPresent()) return new ResponseEntity<>("Product not found", HttpStatus.NOT_FOUND);
        if (!optionalUser.isPresent()) return new ResponseEntity<>("User not found", HttpStatus.NOT_FOUND);
        boolean existsByUserIdAndProductId = favoriteProductRepository.existsByUserIdAndProductId(userId, productId);
        System.out.println(existsByUserIdAndProductId + "---------------------------------------------------------------------------------------------------------------");
        if (existsByUserIdAndProductId)
           return new ResponseEntity<>("This product exist in favorite list", HttpStatus.CONFLICT);
        FavoriteProduct favoriteProduct = new FavoriteProduct();
        favoriteProduct.setProduct(optionalProduct.get());
        favoriteProduct.setUser(optionalUser.get());
        favoriteProductRepository.save(favoriteProduct);
        return new ResponseEntity<>("Successfully saved!", HttpStatus.CREATED);
    }


    public ResponseEntity<?> getAllFavoriteProductByUserId(UUID userId) {
        List<FavoriteProduct> allFavoriteProductsByUserId = favoriteProductRepository.getAllFavoriteProductsByUserId(userId);
        if (allFavoriteProductsByUserId.isEmpty()) {
            return new ResponseEntity<>("Not found", HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(allFavoriteProductsByUserId, HttpStatus.ACCEPTED);
    }


    public ResponseEntity<?> deleteFavoriteProductByProductId(UUID productId, UUID userId) {
        FavoriteProduct favoriteProductsByProduct = favoriteProductRepository.getFavoriteProductsByProduct(productId, userId);
        try {
            favoriteProductRepository.deleteProduct(favoriteProductsByProduct.getId());
        } catch (Exception ignored) {
        }
        return new ResponseEntity<>("Successfully deleted", HttpStatus.ACCEPTED);
    }

}
