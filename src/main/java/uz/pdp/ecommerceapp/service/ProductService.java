package uz.pdp.ecommerceapp.service;

import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import uz.pdp.ecommerceapp.dto.CharacteristicValueDto;
import uz.pdp.ecommerceapp.dto.ProductDto;
import uz.pdp.ecommerceapp.model.*;
import uz.pdp.ecommerceapp.payload.ApiResponse;
import uz.pdp.ecommerceapp.projection.ProductByIdProjection;
import uz.pdp.ecommerceapp.projection.ProductProjection;
import uz.pdp.ecommerceapp.projection.ProductWithCharProjection;
import uz.pdp.ecommerceapp.repository.*;

import javax.transaction.Transactional;
import java.io.IOException;
import java.util.*;

// Bahodir Hasanov 4/11/2022 9:57 PM
@RequiredArgsConstructor
@Service
public class ProductService {

    public final ProductRepository productRepository;
    public final BrandRepository brandRepository;
    public final CategoryRepository categoryRepository;
    public final AttachmentContentRepository attachmentContentRepository;
    public final CharacteristicValueRepository characteristicValueRepository;


    public ResponseEntity<?> getAllProducts(int page, int size, String search, String sort, boolean direction) {
        Pageable pageable = PageRequest.of(
                page - 1,
                size
        );
        try {
            Page<ProductProjection> productByPage = productRepository.findAllProductsByPage(pageable, search);
            return ResponseEntity.ok(new ApiResponse("success", true, productByPage.getContent()));
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new ApiResponse("products not found", false));
        }
    }

    @Transactional
    public ResponseEntity<?> saveProduct(List<MultipartFile> files, ProductDto productDto) {
        Product product = new Product();
        product.setName(productDto.getName());
        UUID brandId = productDto.getBrandId();
        Brand brand = brandRepository.getById(brandId);
        product.setBrand(brand);
        UUID categoryId = productDto.getCategoryId();
        Category category = categoryRepository.getById(categoryId);
        product.setCategory(category);
        Double price = productDto.getPrice();
        product.setPrice(price);
        String description = productDto.getDescription();
        product.setDescription(description);
        List<CharacteristicValue> characteristicsValuesList = new ArrayList<>();
        for (CharacteristicValueDto characteristicValueDto : productDto.getCharacteristicValueDtoList()) {
            UUID characteristicId = characteristicValueDto.getCharacteristicId();
            UUID valueId = characteristicValueDto.getValueId();
            CharacteristicValue characteristicValue = characteristicValueRepository.findCharacteristicValueByCharacteristicIdAndValueId(characteristicId, valueId);
            characteristicsValuesList.add(characteristicValue);
        }
        product.setCharacteristicsValues(characteristicsValuesList);
        List<Attachment> attachments = new ArrayList<>();
        for (MultipartFile file : files) {
            Attachment attachment = new Attachment(file.getName(), file.getContentType(), file.getSize());
            try {
                AttachmentContent attachmentContent = attachmentContentRepository.save(new AttachmentContent(attachment, file.getBytes()));
                attachmentContentRepository.save(attachmentContent);
            } catch (IOException e) {
                e.printStackTrace();
            }
            attachments.add(attachment);
        }
        product.setAttachments(attachments);
        try {
            productRepository.save(product);
            return ResponseEntity.ok(new ApiResponse("successfully saved", true));
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.CONFLICT).body(new ApiResponse("error", false));
        }
    }

    public ResponseEntity<?> getProductById(UUID productId) {

        ProductByIdProjection productProjectionById = productRepository.getProductProjectionById(productId);

        return ResponseEntity.ok(productProjectionById);
    }

    public ResponseEntity<?> getProductWithCharacteristics(UUID productId) {
        ProductWithCharProjection productWithCharacteristics = productRepository.getProductWithCharacteristics(productId);
        return ResponseEntity.ok(productWithCharacteristics);
    }

    public ResponseEntity<?> deleteProductById(UUID productId) {
        try {
            Optional<Product> optionalProduct = productRepository.findById(productId);
            if(!optionalProduct.isPresent()){
                return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new ApiResponse("product not found",false));
            }

            Product product = optionalProduct.get();
            List<UUID> attachmentIdByProductId = productRepository.findAttachmentIdByProductId(productId);
            for (UUID attachmentId : attachmentIdByProductId) {
                AttachmentContent attachmentContentByAttachmentId = attachmentContentRepository.findAttachmentContentByAttachmentId(attachmentId);
                attachmentContentRepository.delete(attachmentContentByAttachmentId);
            }
            productRepository.delete(product);
            return ResponseEntity.ok(new ApiResponse("deleted",true));
        } catch (Exception e){
            return ResponseEntity.status(HttpStatus.CONFLICT).body(new ApiResponse("error",true));
        }
    }
}
