package uz.pdp.ecommerceapp.service;
//Sevinch Abdisattorova 04/13/2022 04:34 AM

import com.stripe.Stripe;
import com.stripe.exception.StripeException;
import com.stripe.model.checkout.Session;
import com.stripe.param.checkout.SessionCreateParams;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import uz.pdp.ecommerceapp.dto.CartItem;
import uz.pdp.ecommerceapp.model.User;
import uz.pdp.ecommerceapp.projection.CustomProductForCart;

import java.util.ArrayList;
import java.util.List;

@Service
public class StripePaymentService {

    @Value("${STRIPE_SECRET_KEY}")
    String stripeApiKey;

    @Value("${BASE_URL}")
    String baseUrl;


    public ResponseEntity<?> createStripeSession(List<CartItem> cartItems, User user) throws StripeException {
        // SUCCESS and FAILURE URLS
        String successURL = baseUrl + "success";
        String failureURL = baseUrl + "failed";

        Stripe.apiKey = stripeApiKey;
        List<SessionCreateParams.LineItem> sessionItemList = new ArrayList<>();

        for (CartItem item : cartItems) {
            sessionItemList.add(createSessionLineItem(item));
        }
        SessionCreateParams params = SessionCreateParams.builder()
                .addPaymentMethodType(SessionCreateParams.PaymentMethodType.CARD)
                .setMode(SessionCreateParams.Mode.PAYMENT)
                .setCancelUrl(failureURL)
                .setSuccessUrl(successURL)
                .addAllLineItem(sessionItemList)
                .setClientReferenceId(user.getId().toString())
                .build();

        Session session = Session.create(params);
        String url = session.getUrl();

        // returning session url
        return ResponseEntity.ok(url);

    }


    private SessionCreateParams.LineItem createSessionLineItem(CartItem item) {
        return SessionCreateParams
                .LineItem
                .builder()
                .setPriceData(createPriceData(item.getProduct()))
                .setQuantity(Long.valueOf(item.getQuantity()))
                .build();
    }


    private SessionCreateParams.LineItem.PriceData createPriceData(CustomProductForCart product) {
        double chargeAmount = (product.getPrice() * 100 + 30) / (1 - 2.9 / 100);
        return SessionCreateParams.LineItem.PriceData.builder()
                .setCurrency("usd")
                .setUnitAmount((long) (chargeAmount))
                .setProductData(SessionCreateParams.LineItem.PriceData.ProductData.builder()
                        .setName(product.getProductName())
                        .build())
                .build();
    }


}
