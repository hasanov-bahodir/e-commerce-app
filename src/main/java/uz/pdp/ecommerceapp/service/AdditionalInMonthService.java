package uz.pdp.ecommerceapp.service;

import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import uz.pdp.ecommerceapp.model.AdditionalInMonth;
import uz.pdp.ecommerceapp.model.Brand;
import uz.pdp.ecommerceapp.repository.AdditionalInMonthRepository;
import uz.pdp.ecommerceapp.repository.BrandRepository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
@RequiredArgsConstructor
public class AdditionalInMonthService {

    private final AdditionalInMonthRepository additionalInMonthRepository;

    public ResponseEntity<?> getAllAdditionalInMonths() {
        List<AdditionalInMonth> additionalInMonthList = additionalInMonthRepository.findAll();
        return ResponseEntity.ok(additionalInMonthList);
    }

    public ResponseEntity<?> getAdditionalInMonthId(UUID id) {
        Optional<AdditionalInMonth> optionalAdditionalInMonth = additionalInMonthRepository.findById(id);
        return ResponseEntity.status(optionalAdditionalInMonth.isPresent()?200:404).body(optionalAdditionalInMonth.get());
    }

    public ResponseEntity<?> editAdditionalInMonth(UUID id, AdditionalInMonth additionalInMonth) {
        Optional<AdditionalInMonth> optionalAdditionalInMonth = additionalInMonthRepository.findById(id);
        if (!optionalAdditionalInMonth.isPresent()) {
            return ResponseEntity.notFound().build();
        }
        AdditionalInMonth editingAdditionalInMonth = optionalAdditionalInMonth.get();
        editingAdditionalInMonth.setMonthNumber(additionalInMonth.getMonthNumber());
        editingAdditionalInMonth.setPercentage(additionalInMonth.getPercentage());
        AdditionalInMonth editedAdditionalInMonth = additionalInMonthRepository.save(editingAdditionalInMonth);
        return ResponseEntity.ok(editedAdditionalInMonth);
    }

    public ResponseEntity<?> addAdditionalInMonth(AdditionalInMonth additionalInMonth) {
        AdditionalInMonth savedAdditionalInMonth = additionalInMonthRepository.save(additionalInMonth);
        return ResponseEntity.ok(savedAdditionalInMonth);
    }

    public ResponseEntity<?> deleteAdditionalInMonth(UUID id) {
        try {
            additionalInMonthRepository.deleteById(id);
            return ResponseEntity.ok("successfully deleted");
        } catch (Exception e){
            e.printStackTrace();
        }
        return ResponseEntity.notFound().build();
    }
}
