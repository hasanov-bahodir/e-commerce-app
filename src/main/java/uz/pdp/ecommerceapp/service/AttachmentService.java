package uz.pdp.ecommerceapp.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import uz.pdp.ecommerceapp.controller.AttachmentController;
import uz.pdp.ecommerceapp.model.Attachment;
import uz.pdp.ecommerceapp.model.AttachmentContent;
import uz.pdp.ecommerceapp.payload.ApiResponse;
import uz.pdp.ecommerceapp.repository.AttachmentContentRepository;
import uz.pdp.ecommerceapp.repository.AttachmentRepository;

import java.util.UUID;

// Bahodir Hasanov 4/14/2022 5:47 AM

@Service
public class AttachmentService {
    @Autowired
    AttachmentRepository attachmentRepository;
    @Autowired
    AttachmentContentRepository attachmentContentRepository;


    public ResponseEntity<?> getPhotoById(UUID photoId) {
        try {
            AttachmentContent attachmentContentByAttachmentId = attachmentContentRepository.findAttachmentContentByAttachmentId(photoId);
            return ResponseEntity.ok(new ApiResponse("success", true, attachmentContentByAttachmentId.getData()));
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new ApiResponse("error", false));
        }
    }
}
